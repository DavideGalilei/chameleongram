import os
import shutil

from tl_parser.parser import parsed, generate_files


def copytree(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copytree(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                shutil.copy2(s, d)


def generate(show_banner: bool = False):
    generate_files(show_banner=show_banner)

    copytree(parsed / "core", "chameleongram/raw/core")
    copytree(parsed / "functions", "chameleongram/raw/functions")
    copytree(parsed / "types", "chameleongram/raw/types")
    shutil.copyfile(parsed / "_all.py", "chameleongram/raw/_all.py")

    shutil.rmtree(parsed)
