import os
import pathlib
import re
from collections import defaultdict

path = pathlib.Path(os.path.dirname(os.path.abspath(__file__)))
res = path / "res"
template = open(res / "template").read()
imports = open(res / "imports").read()
schema = open(res / "api.tl").read()
mtproto = open(res / "mtproto.tl").read()
_all = open(res / "_all").read()

parsed = path / "parsed"

obj_re = re.compile(r"^([\w.]+)#([0-9a-f]+)\s.*=\s.*;$")
args_re = re.compile(r"([\w{]+):([\w.<>?#!}]+)")
flags_re = re.compile(r"^flags\.(\d+)\?([\w.<>]+)$")
vector_re = re.compile(r"^vector<([\w.]+)>$", re.IGNORECASE)

PRIMITIVES = (
    "Bool",
    "bytes",
    "double",
    "int",
    "int128",
    "int256",
    "long",
    "string",
    "true",
)
TLObject = "TL"  # Class name to inherit from the base object


def cap(text: str) -> str:
    """Capitalizes the first letter of a string"""

    return "".join(elem[0].upper() + elem[1:] for elem in text.split("_"))


def parse_args(args: list) -> list:
    """A function for parsing arguments"""

    args = list(args)
    for i, arg in enumerate(args):
        if arg[0][0] == "{":
            args.remove(arg)
            try:
                arg = args[i]
            except IndexError:
                continue
        arg = list(arg)
        flag_index = None
        if flag_match := flags_re.match(arg[1]):
            flag_groups = flag_match.groups()
            arg[1] = flag_groups[1]
            flag_index = int(flag_groups[0])
        if vector_match := vector_re.match(arg[1]):
            arg[1] = vector_match.groups()[0]
        real_type = arg[1]
        init_type = arg[1]
        if init_type in PRIMITIVES:
            if init_type in ("int", "int128", "int256", "long"):
                arg[1] = cap(arg[1])
                init_type = "int"
            elif init_type == "double":
                arg[1] = "Double"
                init_type = "float"
            elif init_type == "string":
                arg[1] = "String"
                init_type = "str"
            elif init_type in ("Bool", "true"):
                arg[1] = "Bool"
                init_type = "bool"
            elif init_type == "bytes":
                arg[1] = "Bytes"
        else:
            arg[1] = TLObject
            init_type = TLObject
        arg = (*arg, init_type, flag_match, flag_index, vector_match, real_type)
        args[i] = arg
    return args


def parse_init(args: list) -> str:
    """A function for generating "__init__" args"""

    return ", " + ", ".join(
        (
            *(
                f"{arg[0]}: {f'List[{arg[2]}]' if arg[5] else arg[2]}"
                for arg in args
                if not arg[3] and arg[0] != "flags"
            ),
            *(
                f"{arg[0]}: {f'List[{arg[2]}]' if arg[5] else arg[2]} = None"
                for arg in args
                if arg[3] and arg[0] != "flags"
            ),
        )
    )


def parse_init_code(args: list) -> str:
    """A function for generating "__init__" code"""

    return "\n        ".join(
        f"cls.{arg[0]} = {arg[0]}" for arg in args if arg[0] != "flags"
    )


def parse_read(args: list) -> str:
    """A function for generating "read" code"""

    read_body = [
        "flags = Int.read(data)"
        if arg[0] == "flags"
        else f"{arg[0]} = {'data.getobj()' if arg[1] == TLObject else ('True' if arg[6] == 'true' else f'{arg[1]}.read(data)')} if flags & {(1 << arg[4])} else {('[]' if arg[5] else 'None') if arg[1] == TLObject else ('False' if arg[6] == 'true' else 'None')}"
        if arg[3]
        else f"{arg[0]} = data.getobj({arg[1] * (arg[1] != TLObject)})"
        if arg[5]
        else f"{arg[0]} = {'data.getobj()' if arg[1] == TLObject else arg[1] + '.read(data)'}"
        for arg in args
    ]

    return "\n        ".join(read_body) + "\n        "


def parse_getvalue(args: list) -> str:
    """A function for generating "getvalue" code"""

    normal_body = []
    flags_body = []
    for arg in args:
        if arg[0] == "flags":
            flags_body.append("flags = 0")
            flags_body.append("data.write(Int.getvalue(flags))")
        elif arg[3]:
            flags_body.insert(
                1, f"flags |= {(1 << arg[4])} if cls.{arg[0]} is not None else 0"
            )
            if arg[5]:
                normal_body.append(
                    f"\n        if cls.{arg[0]} is not None:\n            data.write(Vector().getvalue(cls.{arg[0] if arg[1] == TLObject else f'{arg[0]}, {arg[1]}'}))"
                )
            elif arg[1] != "Bool":
                normal_body.append(
                    f"\n        if cls.{arg[0]} is not None:\n            data.write({f'cls.{arg[0]}.getvalue()' if arg[1] == TLObject else f'{arg[1]}.getvalue(cls.{arg[0]})'})"
                )
        elif arg[5]:
            normal_body.append(
                f"data.write(Vector().getvalue(cls.{arg[0] if arg[1] == TLObject else f'{arg[0]}, {arg[1]}'}))"
            )
        else:
            normal_body.append(
                f"data.write({f'cls.{arg[0]}.getvalue()' if arg[1] == TLObject else f'{arg[1]}.getvalue(cls.{arg[0]})'})"
            )
    flags_body[1:-1] = list(reversed(flags_body[1:-1]))
    return "\n        ".join(flags_body + normal_body) + "\n        "


def parse(lines: str, core_mode: bool = False):
    mode: str = "types"
    _tmp_all = []
    _tmp = defaultdict(lambda: defaultdict(list))
    for line in (
            line.strip()
            for line in lines.split("\n")
            if all((line.strip(), not line.strip()[:0] == "/"))
    ):
        if line in ("---types---", "---functions---"):
            mode = "functions" if line == "---functions---" else "types"
        match = obj_re.match(line)
        if not match:
            continue
        _tmp_all += [(*match.groups(), mode, line)]
    for _type in ["types", "functions"] + (["core"] if core_mode else []):
        for match in filter(lambda _: _[2] == _type, _tmp_all):
            namespace, name = match[0].split(".")[0] if "." in match[0] else "", cap(
                match[0].split(".")[-1] if "." in match[0] else match[0]
            )
            parsed_args = parse_args(args_re.findall(match[3]))
            _tmp_init_code = parse_init_code(parsed_args)
            _tmp[_type][namespace if namespace else "__init__"] += [
                (
                    template
                    % (
                        name,  # class name
                        match[1],  # ID
                        parse_init(parsed_args),  # init args
                        _tmp_init_code if _tmp_init_code else "...",  # init code
                        name,  # read type hint
                        parse_read(parsed_args),  # read code
                        name,  # read return
                        ", ".join(
                            f"{arg[0]}={arg[0]}"
                            for arg in parsed_args
                            if arg[0] != "flags"
                        ),
                        # read return args
                        parse_getvalue(parsed_args),  # getvalue code
                    ),
                    (namespace, name),
                )
            ]
    for key, value in _tmp.items():
        _tmp_imports = defaultdict(list)
        for k, v in value.items():
            if k != "__init__":
                with open(
                        (parsed / "core" if core_mode else parsed) / f"{key}/{k}.py", "w+"
                ) as f:
                    f.write("\n\n".join((imports, *(elem[0] for elem in v))))
                _tmp_imports[k] += (elem[1] for elem in v)
        with open(
                (parsed / "core" if core_mode else parsed) / f"{key}/__init__.py", "w+"
        ) as f:
            f.write(
                "\n\n".join(
                    (
                        imports
                        + "\n".join(
                            f"from .{elem[0]} import {', '.join(x[1] for x in elem[1])}"
                            for elem in _tmp_imports.items()
                        ),
                        *(x[0] for x in value["__init__"]),
                    )
                )
            )
    if core_mode:
        with open(parsed / "core/__init__.py", "w+") as f:
            f.write("from . import functions, types\n")
    for match in sorted(_tmp_all, key=lambda _: f"{_[2]}.{_[0]}"):
        yield f"""0x{match[1]}: {'core.' * core_mode}{f'{match[2]}.' * bool(match[2])}{cap(match[0]) if not len(match[0].split('.')) - 1 else f"{''.join(match[0].split('.')[:-1])}.{cap(match[0].split('.')[-1])}"}"""
    if __name__ == "__main__":
        print(f"Done parsing (core_mode={core_mode})")


def generate_files(show_banner: bool = False):
    if show_banner:
        print("Python TL Schema parser, Copyright (C) 2020, Davide Galilei")
    for directory in (
            parsed,
            *(parsed / elem for elem in ("types", "functions")),
            parsed / "core" / "types",
            parsed / "core" / "functions",
    ):
        if not os.path.isdir(directory):
            if os.path.exists(directory):
                os.remove(directory)
            os.makedirs(directory)
    # Generates _all.py
    with open(parsed / "_all.py", "w+") as f:
        f.write(
            _all
            % (
                "from . import core, types, functions",
                ",\n    ".join((*parse(schema), *parse(mtproto, core_mode=True))),
            )
        )


if __name__ == "__main__":
    generate_files(show_banner=True)
